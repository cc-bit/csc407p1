#|| 
Chris Chitwood
CSC 407
Program 1
Due: 2/5/21
||#



;;; QuickSort

#|| F# equivalent
let rec quicksort = function
    x::xs ->
        let low, high = List.partition ((>=) x) xs
        List.concat [quicksort low; [x]; quicksort high]
    | _ -> []
||#

(defun quicksort (curList &aux (pivot (car curList)));; &aux is used of the lambdas. curList is the current list.
 (if (cdr curList);; Rest of the current list. 
  (nconc (quicksort (remove-if-not #'(lambda(x)(< x pivot))curList));; This will handle the items with a lower value than the pivot.
         (remove-if-not #'(lambda (x) (= x pivot)) curList);; This is for the pivot point for the quicksort
         (quicksort (remove-if-not #'(lambda (x) (> x pivot))curList)));; This will and the itme with a higher value than the pivot.
  curList))



;;; InsertionSort
(defun insert-helper (temp remainingList)
  (if (null remainingList)
    (cons temp nil) ;; Return a list with the temp value(lists in lisp end with nil).
    (if (<= temp (car remainingList));; Check if the temp value is less than or equal to the first item in the remaing list.
      (cons temp remainingList);; If it is, construct and return a list with the temp value and the remaining list.
      
      ;; Otherwise construct and return a list with the first value in the remaining list and a recursive call to the insert-helper.
      (cons (car remainingList) (insert-helper temp (cdr remainingList))))))

(defun insertionsort (curList)
  (if (null curList)
    nil;; Result if the current list is empty
    
    ;; Send the first item from the current list to the insert-helper along with a recusive call on insertion-sort to run through the rest of the current list
    (insert-helper (car curList) (insertionSort (cdr curList)))))
   

;;;BubbleSort
;; Could have also used car and cadr instead of nth, but I wanted to use nth to make it obvious to myself what I'm doing when I read it over.
(defun bubblesort (list)
  (loop repeat (1- (length list)) do;; Go through length of list.
    (loop for current on list while (cdr current) do
      (if (> (nth 0 current) (nth 1 current));; Check a pair of values for when the first value is greater than the second.
      	(progn ;; This should swap the values
      	  (defvar temp 0);; Make temp var
      	  (setf temp (nth 0 current));; Place the first value in temp.
      	  (setf (nth 0 current) (nth 1 current));; Put second value in place of first.
      	  (setf (nth 1 current) temp)))));; Put the first value in place of second.
  list)



;;; SelectionSort

(defun select-helper (curList)
  (cond ((null (cdr curList)) (car curList));; A branch for when the rest of the current list is empty.
    (t (let ((curSelect (select-helper (cdr curList))));; Use a recursive call on the helper with the rest fo the current list.
      (if (< (car curList) curSelect) (car curList) curSelect)))));; Check if the first value of the current list is less than the current selection.

(defun select-remove (curSelect curList)
  (remove curSelect curList :count 1);; Remove the current item from the list; however, only do it once.
)
 
(defun selectionsort (curList)
  (cond ((null curList) ());; Accounting for a null current list.
    (t (let ((curSelect (select-helper curList)));; Otherwise, let the current selection be the return of the helper.
      ;; Construct a list with the current selectiona and the recurive call while removing the current selection from the list.
      (cons curSelect (selectionsort (select-remove curSelect curList)))))))



;;; MergeSort
(defun merge-helper(list1 list2)
    (cond
      ;; Branches for list lengths of zero
      ((= (list-length list1) 0) list2)
      ((= (list-length list2) 0) list1)
      
      ;; When the first item in first list is greater than the first item in the second, append the recursive call of the helper to the list returned from nth.
      ((> (nth 0 list1) (nth 0 list2)) (append (list (nth 0 list2)) (merge-helper list1 (cdr list2))))
      ((< (nth 0 list1) (nth 0 list2)) (append (list (nth 0 list1)) (merge-helper (cdr list1) list2)))
      ((= (nth 0 list1) (nth 0 list2)) (append (list (nth 0 list1) (nth 0 list2)) (merge-helper (cdr list1) (cdr list2))))))

(defun mergesort(curList)
  (let ((length (list-length curList)))
    (cond ((= length 1) curList);; Account for the list being of length 1
      (t (merge-helper (mergesort (subseq curList 0 (ceiling (/ length 2))));;Otherwise, use the merge-helper with a recurisve call to mergesort
        (mergesort (subseq curList (ceiling (/ length 2)))))))));; Subseq used to make a smaller list from the current to half the length.

